/*
昇順・降順を標準入力から指定して英語の月の名前をソート

# 以下の関数を作成・使用しなければならない
void aPrint(char month[][20], int n)
ソート前、ソート後の月の名前が入った配列の表示

void swap(char month[][20], int i, int j)
配列上の文字列の入れ替え

void sort(char month[][20], int n, int order)
配列のソート
ソートの方法はどんな方法を使用しても良い
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define array_size(array) sizeof(array)/sizeof(array[0])
#define ORDER_ASC  0	// 昇順
#define ORDER_DESC 1	// 降順

void aPrint(char month[][20], int n)
{
	int i;
	for (i = 0; i < n; i++)
	{
		printf("%s\n", month[i]);
	}
}

void swap(char month[][20], int i, int j)
{
	char tmp[20];
	tmp[19] = month[i][19] = month[j][19] = 0;
	strncpy(tmp, month[i], 19);
	strncpy(month[i], month[j], 19);
	strncpy(month[j], tmp, 19);
}

void sort_main(char* month[], int n, int order)
{
	int i, j;
	int lp, rp;
	char **left, **right;
	int left_c, right_c;
	if (n >= 2)
	{
		j = 0;
		
		left_c = n/2;
		left = (char**)malloc(sizeof(char*) * left_c);
		for (i = 0; i < left_c; i++) left[i] = month[j++];

		right_c = n-n/2;
		right = (char**)malloc(sizeof(char*) * right_c);
		for (i = 0; i < right_c; i++) right[i] = month[j++];

		sort_main(left, left_c, order);
		sort_main(right, right_c, order);

		i = lp = rp = 0;
		while (lp < left_c && rp < right_c)
		{
			if (order == ORDER_ASC ?
					strcmp(left[lp], right[rp]) < 0 :
					strcmp(left[lp], right[rp]) > 0)
			{
				month[i] = left[lp++];
			}
			else
			{
				month[i] = right[rp++];
			}
			i++;
		}
		while (lp < left_c)  month[i++] = left[lp++];
		while (rp < right_c) month[i++] = right[rp++];

		free(left);
		free(right);
	}
}

void sort(char month[][20], int n, int order)
{
	int i;
	char** month_ptrs;
	char* month_tmp;
	
	// ソート
	month_ptrs = (char**)malloc(n * sizeof(char*));
	for (i = 0; i < n; i++) month_ptrs[i] = month[i];
	sort_main(month_ptrs, n, order);

	// 元の配列に書き戻し
	month_tmp = (char*)malloc(n * sizeof(char) * 20);
	for (i = 0; i < n; i++)
	{
		strncpy(&month_tmp[i*20], month_ptrs[i], 19);
		month_tmp[i*20+19] = 0;
	}
	memcpy(month, month_tmp, n * sizeof(char) * 20);

	// 後片付け
	free(month_ptrs);
	free(month_tmp);
}

int main()
{
	char month[][20] = {
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December",
	};
	int type;
	printf("%d:昇順 %d:降順 を入力: ", ORDER_ASC, ORDER_DESC);
	while(1)
	{
		scanf("%d", &type);
		rewind(stdin);
		if (type == ORDER_ASC || type == ORDER_DESC) break;
		printf("%dか%dを入力してください\n", ORDER_ASC, ORDER_DESC);
	}
	sort(month, 12, type);
	aPrint(month, 12);
	return 0;
}

