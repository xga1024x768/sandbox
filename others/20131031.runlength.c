/*
 * 文字列をランレングス法で圧縮するプログラム
 *
 * * コマンドライン引数で-cを指定することで圧縮モード、-dを指定することで伸長モードになる。
 *   2つ以上指定された場合、後ろに指定されたモードが指定されたと判断する。
 *   -cも-dも与えられなかった場合、圧縮モードで動作する。
 * * 圧縮/伸長する文字列は標準入力経由で入力する。
 * * 圧縮時は画面に表示可能な文字が使用出来るが、数字が入力されたらエラー。
 *   (繰り返し回数を表現するために数字を使用するため)
 * * 伸長時は画面に表示可能な文字以外が入力されたらエラー。
 */

#include <stdio.h>
#include <ctype.h>
#include <string.h>

// 圧縮する最小文字数
#define MIN_COMPRESS_LENGTH 2

void compress_output_string(char c, unsigned int repeat)
{
	if (repeat >= MIN_COMPRESS_LENGTH)
	{
		printf("%c%u", c, repeat);
	}
	else
	{
		while (repeat--)
		{
			putchar(c);
		}
	}
}

void compress()
{
	int c;
	char pc = 0;	// 前の文字
	unsigned int repeat;	// 繰り返し回数

	while ((c=getchar()) != '\n')
	{
		if (!isprint(c) || isdigit(c))
		{
			// 無効な文字が入力されいたらエラーを表示して終了
			fprintf(stderr, "エラー: 数字など使用できない文字が使われました。");
			goto compress_fin;
		}

		if (pc == 0)
		{
			// ループ始まって最初の文字なのでpcとrepeatを初期化する
			pc = c;
			repeat = 1;
		}
		else
		{
			if (pc != c)
			{
				// 前のループと違う文字が来たので前のループで入力されていた文字を画面に表示する
				compress_output_string(pc, repeat);
				// pcとrepeatを初期化
				pc = c;
				repeat = 1;
			}
			else
			{
				// 前のループと同じ文字だったので繰り返し回数を増やす
				repeat++;
			}
		}
	}
	// 最後のループで入力されていた文字を表示
	compress_output_string(pc, repeat);

compress_fin:
	printf("\n");
}

void decompress_output_string(char c, unsigned int repeat)
{
	if (repeat == -1) repeat = 1;
	while (repeat--)
	{
		putchar(c);
	}
}

void decompress()
{
	int c;
	int pc = 0;
	unsigned int repeat = 0;	// 繰り返し回数

	while ((c=getchar()) != '\n')
	{
		if (!isprint(c))
		{
			// 無効な文字が入力されいたらエラーを表示して終了
			fprintf(stderr, "エラー: 使用できない以外が使われました。");
			goto decompress_fin;
		}

		if (isdigit(c))
		{
			if (pc == 0)
			{
				// 1文字目から数字が来るのはおかしいのでエラーを表示して終了
				fprintf(stderr, "エラー: 不正な文字です。");
				goto decompress_fin;
			}
			if (repeat == -1) repeat = 0;
			repeat *= 10;
			repeat += c-'0';
		}
		else
		{
			if (pc)
			{
				decompress_output_string(pc, repeat);
			}
			pc = c;
			repeat = -1;
		}
	}
	decompress_output_string(pc, repeat);

decompress_fin:
	printf("\n");
}

// コマンドライン引数に指定されたモードを取得する関数
int cmdline_is_compress(int argc, char* argv[])
{
	int compress = 1;
	while (--argc > 0)
	{
		if (strcmp(argv[argc], "-c") == 0)
		{
			break;
		}
		else if (strcmp(argv[argc], "-d") == 0)
		{
			compress = 0;
			break;
		}
	}
	return compress;
}

int main(int argc, char* argv[])
{
	if (cmdline_is_compress(argc, argv))
	{
		// 圧縮モード
		compress();
	}
	else
	{
		// 伸長モード
		decompress();
	}
}

