// ボイヤームーア法で文字列探索
// string.hは使用禁止

#include <stdio.h>

int strlen(char* str)
{
	if (str[0] == 0) return 0;
	return strlen(str+1) + 1;
}

int find_string(char* src, char* find)
{
	int srclen = strlen(src);	// 検索対象文字列の長さ
	int findlen = strlen(find);	// 検索する文字列の長さ
	int findlast = findlen-1;	// 検索する文字列の最後の要素の添字
	char partlast;				// 部分文字列の最後の文字
	int i, j;

	i = 0;
	while (i < srclen)
	{
		// 部分文字列の後ろから比較
		for (j = findlast; (j >= 0) && (src[i+j] == find[j]); j--);

		// 見つかったみたいなのでその位置を返す
		if (j == -1) return i;

		// iをいくつ増やすかを後ろから辿って決める
		// 一番最後の文字は無限ループになるので無視する
		partlast = src[i+findlast];
		for (j = findlast-1; (j >= 0) && (partlast != find[j]); j--);

		// 部分文字列の最後の文字が探索文字列に存在すればその位置を、
		// そうでなければ探索文字列の数だけiを増やす
		i += (j >= 0) ? findlast-j : findlen;
	}

	return -1;
}

int main()
{
	char src[1024] = {0};
	char find[768] = {0};
	int result;

	printf("検索対象文字列: ");
	fgets(src, 1024, stdin);

	printf("検索する文字列: ");
	fgets(find, 768, stdin);
	find[strlen(find)-1] = 0;

	result = find_string(src, find);
	if (result != -1)
		printf("%d文字目に\"%s\"を見つけました。\n", result+1, find);
	else
		printf("\"%s\"は見つかりませんでした。\n", find);

	return 0;
}

