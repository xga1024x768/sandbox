#include <stdio.h>

void show_array(size_t size, int* data)
{
	int i;
	for (i = 0; i < size; i++)
		printf("%d ", data[i]);
}

void swap(int* a, int* b)
{
	if (*a != *b)
	{
		*a ^= *b;
		*b ^= *a;
		*a ^= *b;
	}
}

void sort_swap(size_t size, int* data)
{
	if (data[0] > data[1]) swap(&data[0], &data[1]);
	if (size > 2) sort_swap(size-1, &data[1]);
}

void sort(size_t size, int* data)
{
	sort_swap(size, data);
	if (size > 2) sort(size-1, data);
}

int main()
{
	int data[] = {83, 92, 84, 39, 72, 47, 93, 82, 58, 92};

	printf("Original: ");
	show_array(sizeof(data)/sizeof(data[0]), data);
	printf("\n");

	sort(sizeof(data)/sizeof(data[0]), data);

	printf("Sorted:   ");
	show_array(sizeof(data)/sizeof(data[0]), data);
	printf("\n");

	return 0;
}

