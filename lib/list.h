#ifndef _H_LIST_
#define _H_LIST_

// 双方向リスト

// メモリ解放に標準Cライブラリ内のfree()を使用する
#define LIST_USE_LIBC_FREE ((void (*)(void*))  0)
// list_*()関数でのメモリ解放を禁止する
#define LIST_FORBIT_FREE   ((void (*)(void*)) -1)

typedef struct ListItem
{
	struct ListItem* prev;
	struct ListItem* next;
	void* data;
	void (*free)(void*);
} *ListItem;

typedef struct List
{
	ListItem first;
	ListItem last;
	size_t   count;
} *List;

List list_init();
void list_free(List, int);
void* list_get(List, int);
void list_append(List, void*);
void list_append2(List, void*, void (*free)(void*));
//void list_insert(List, int, void*);
void list_remove(List, int, int);
int list_count(List);

typedef int (*list_each_callback)(int, void*, void*);
void list_each(List, list_each_callback, void*);

#endif
