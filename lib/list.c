#include <stdlib.h>

#include "list.h"

// リストの初期化
// 返り値
// 　初期化されたリスト
List list_init()
{
	List list = (List)malloc(sizeof(struct List));
	list->first = NULL;
	list->last  = NULL;
	list->count = 0;
	return list;
}

// リストの解放
// 引数
// 　list
// 　　リスト
// 　do_free
// 　　0　　: リストに入れたデータのメモリを開放しない
// 　　0以外: リストに入れたデータのメモリを解放する
void list_free(List list, int do_free)
{
	ListItem cur, next;
	while (list_count(list)) list_remove(list, 0, do_free);
	free(list);
}

// リストの指定した位置のデータを取得
// 引数
// 　list
// 　　リスト
// 　pos
// 　　位置
// 返り値
// 　データ
// 　指定した位置にデータが存在しない場合NULL
void* list_get(List list, int pos)
{
	int i;
	ListItem item;
	for (item = list->first, i = 0; item && i < pos; item = item->next, i++);
	return item ? item->data : NULL;
}

// リストの末尾にデータを追加
// 引数
// 　list
// 　　リスト
// 　data
// 　　追加するデータ
void list_append(List list, void* data)
{
	list_append2(list, data, LIST_USE_LIBC_FREE);
}

// リストの末尾にデータを追加
// 引数
// 　list
// 　　リスト
// 　data
// 　　追加するデータ
// 　free
// 　　メモリ解放時に使用する関数へのポインタ
// 　　あるいは以下の定数
// 　　LIST_USE_LIBC_FREE: メモリ解放に標準Cライブラリのfree()関数を使用する
// 　　LIST_FORBIT_FREE:   list_*()関数によるメモリ解放を禁止する
void list_append2(List list, void* data, void (*free)(void*))
{
	ListItem item = (ListItem)malloc(sizeof(struct ListItem));
	if (list->last == NULL)
	{
		item->prev  = NULL;
		item->next  = NULL;
		list->first = item;
		list->last  = item;
	}
	else
	{
		item->prev = list->last;
		item->next = NULL;
		list->last->next = item;
		list->last       = item;
	}
	item->data = data;
	item->free = free;
	list->count++;
}

// リストからデータを削除
// 引数
// 　list
// 　　リスト
// 　pos
// 　　削除するデータの位置
// 　do_free
// 　　0　　: データのメモリを開放しない
// 　　0以外: データのメモリを解放する
void list_remove(List list, int pos, int do_free)
{
	int i;
	ListItem item;

	// 指定された位置のデータを探す
	for (item = list->first, i = 0; item && i < pos; item = item->next, i++);
	if (item)
	{
		// 指定された位置にデータを見つけた

		// next, prevをつなぎかえる
		if (item->prev) item->prev->next = item->next;
		else            list->first = item->next;
		if (item->next) item->next->prev = item->prev;
		else            list->last = item->prev;

		// メモリ解放
		if (do_free)
		{
			switch ((long)item->free)
			{
				case (long)LIST_USE_LIBC_FREE:
					free(item->data);
					break;
				case (long)LIST_FORBIT_FREE:
					// なにもしない
					break;
				default:
					item->free(item->data);
					break;
			}
		}
		free(item);
		list->count--;
	}
}

// リストに入っているデータの数を取得
// 引数
// 　list
// 　　リスト
// 返り値
// 　リストに入っているデータ数
int list_count(List list)
{
	return list->count;
}

// リストのデータを順番に処理する
// 引数
// 　list
// 　　リスト
// 　callback
// 　　データを処理する関数
// 　extra
// 　　データを処理する関数に渡したいデータへのポインタ
void list_each(List list, list_each_callback callback, void* extra)
{
	int i;
	ListItem item;
	for (item = list->first, i = 0; item && callback(i, item->data, extra); item = item->next, i++);
}

