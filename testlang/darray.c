#include "darray.h"

#include <stdlib.h>

struct DArray* darray_init()
{
	struct DArray* array;
	array = (struct DArray*)malloc(sizeof(struct DArray) + sizeof(void*) * DARRAY_COUNT);
	array->size = DARRAY_COUNT;
	array->used = 0;
	return array;
}

struct DArray* darray_realloc(struct DArray* array, unsigned int size)
{
	if (size%DARRAY_COUNT) size = size / DARRAY_COUNT + DARRAY_COUNT;
	array->size += size;
	return realloc(array, sizeof(struct DArray) + sizeof(void*) * array->size);
}

void darray_free(struct DArray* array)
{
	free(array);
}

void darray_free_alldata(struct DArray* array)
{
	int i;
	for (i = 0; i < array->used; i++) free(array->data[i]);
}

unsigned int darray_push(struct DArray** array, void* data)
{
	if ((*array)->used+1 > (*array)->size)
	{
		*array = darray_realloc(*array, DARRAY_COUNT);
	}
	(*array)->data[(*array)->used] = data;
	(*array)->used++;
	return (*array)->used-1;
}

void* darray_get(struct DArray* array, unsigned int pos)
{
	if (pos > array->used) return NULL;
	return array->data[pos];
}

