#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "darray.h"
#include "list.h"

char* tokenString(char* start, char* end)
{
	char* str;
	str = (char*)malloc(end-start+1);
	strncpy(str, start, end-start+1);
	return str;
}

int tokenizer(struct DArray** tokens, char* source)
{
	char* c;
	char* start;			// トークンの開始位置
	unsigned int length;	// トークン文字列の長さ
	for (start = c = source; *c != 0; c++)
	{
		if (*c == ' ' || *c == '\t' || *c == '\n')
		{
			// トークンの区切りになる半角スペースかタブが来た
			if (*start == *c)
			{
				start = c;
			}
			else
			{
				darray_push(tokens, tokenString(start, c-1));
				start = c+1;
			}
		}
		else if (*c == '"')
		{
			// 最初の"をトークンに追加
			darray_push(tokens, tokenString(start, c));
			// 次の"が来るまでの文字列を取得
			for (start = ++c; *c != '"'; c++);
			darray_push(tokens, tokenString(start, c-1));
			// 最後の"をトークンに追加
			darray_push(tokens, tokenString(c, c));
			start = c+1;
		}
		if (*c == '\n')
		{
			// 文の区切り
			darray_push(tokens, tokenString(c, c));
			start = c+1;
		}
	}
}

#define RUN_SUCCESS             0
#define RUN_INVALID_INSTRUCTION -1

int run(struct DArray* tokens)
{
	unsigned int i;
	for (i = 0; i < tokens->used; i++)
	{
		if (strcmp(tokens->data[i], "puts") == 0)
		{
			if (*(char*)(tokens->data[++i]) == '"')
			{
				puts(tokens->data[++i]);
				i+=2;
			}
		}
		else
		{
			if (*(char*)(tokens->data[i]) != '\n')
			{
				return RUN_INVALID_INSTRUCTION;
			}
		}
	}
	return RUN_SUCCESS;
}

int main()
{
	struct DArray* tokens;
	char buf[1024];
	unsigned int i;

	tokens = darray_init();
	while (fgets(buf, 1024, stdin) != 0)
	{
		tokenizer(&tokens, buf);
	}

	run(tokens);

#ifdef DEBUG
	for (i = 0; i < tokens->used; i++)
	{
		printf("[%s]", (char*)darray_get(tokens, i));
	}
#endif

	darray_free_alldata(tokens);
	darray_free(tokens);

	return 0;
}

