#ifndef _H_DARRAY_
#define _H_DARRAY_

#define DARRAY_COUNT 16

struct DArray
{
	unsigned int size;
	unsigned int used;
	void* data[0];
};

struct DArray* darray_init();
void darray_free(struct DArray*);
void darray_free_alldata(struct DArray*);
unsigned int darray_push(struct DArray**, void*);
void* darray_get(struct DArray*, unsigned int);

#endif

