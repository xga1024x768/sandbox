#ifndef _H_CHALLENGERESPONSE_
#define _H_CHALLENGERESPONSE_

// チャレンジコードの長さ
// 最後にNULLが入るので実際にはここに書かれた数字-1の長さになる
#define CHALLENGE_CODE_LENGTH (128+1)

// レスポンスコードの長さ
// SHA1の場合20バイト
#define RESPONSE_CODE_LENGTH  20

#endif

