// サーバ側の処理

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#include "challengeresponse.h"
#include "common.h"
#include "server.h"

// ユーザ一覧
// 最後はNULLで終わらせること
char* users[][2] =
{
	//{"ユーザID", "パスワード"},
	{"user1", "password"},
	{NULL, NULL},
};

/*
 * 指定したユーザIDのユーザのパスワードを取得
 * 引数:
 * 　id: ユーザID
 * 返り値:
 * 　ユーザのパスワード
 * 　ユーザが存在しなかった場合NULL
 */
char* get_password(char* id)
{
	int i;
	for (i = 0; users[i][0] && strcmp(users[i][0], id); i++);
	return users[i][1];
}

/*
 * チャレンジコードの生成
 * 引数:
 * 　[out] challenge_code: チャレンジコード
 */
void generate_challenge_code(char challenge_code[CHALLENGE_CODE_LENGTH])
{
	int i;
	char r;
	srand(time(NULL));
	for (i = 0; i < CHALLENGE_CODE_LENGTH; i++)
	{
		r = rand();
		if (r == 0)
		{
			// 0が含まれていると不都合なので乱数生成し直す
			i--;
			continue;
		}
		challenge_code[i] = r;
	}
	challenge_code[i] = 0;
}

/*
 * ログインセッションの開始
 * 引数:
 * 　sess: ログインセッションへのポインタ
 */
void server_start_loginsession(struct LoginSession** sess)
{
	*sess = (struct LoginSession*)malloc(sizeof(sess));
	if (!*sess)
	{
		fprintf(stderr, "メモリ確保に失敗しました。\n");
		exit(EXIT_FAILURE);
	}
	memset(*sess, 0, sizeof(**sess));
}

/*
 * ログインセッションの終了
 * 引数:
 * 　sess: ログインセッション
 */
void server_finish_loginsession(struct LoginSession* sess)
{
	free(sess);
}

/*
 * チャレンジコードの要求
 * 引数:
 * 　sess: ログインセッション
 * 　id:   ユーザID
 * 　[out] challenge_code: チャレンジコード
 * 返り値:
 * 　1の場合正常終了
 * 　0の場合指定したIDのユーザが存在しなかった
 */
int server_request_challenge_code(struct LoginSession* sess, char* id, char challenge_code[CHALLENGE_CODE_LENGTH])
{
	char* pass;

	// ユーザのパスワードを取得
	// NULLであればユーザが存在しないのでエラーを返す
	pass = get_password(id);
	if (!pass) return 0;

	// チャレンジコードの生成
	generate_challenge_code(challenge_code);

	// レスポンスコードを生成
	generate_response_code(challenge_code, pass, sess->response_code);

	return 1;
}

/*
 * レスポンスコードの送信
 * 実際にログインできたかどうかはserver_session_login_status()で確認すること
 * 引数:
 * 　sess: ログインセッション
 * 　response_code: レスポンスコード
 * 返り値:
 * 　0の時レスポンスコードが未生成または破棄された状態
 * 　1の時ログイン処理が成功 (ログイン成功したわけではない)
 */
int server_send_response_code(struct LoginSession* sess, char response_code[RESPONSE_CODE_LENGTH])
{
	int i, flg;

	// レスポンスコードが生成されているか確認
	for (i = 0, flg = 0; i < RESPONSE_CODE_LENGTH; i++)
	{
		if (response_code[i] != 0)
		{
			flg = 1;
			break;
		}
	}
	if (!flg) return 0;

	// レスポンスコードが一致しているか確認
	if (memcmp(sess->response_code, response_code, RESPONSE_CODE_LENGTH) == 0)
	{
		// 一致しているのでログイン成功
		sess->status = 1;
	}

	// レスポンスコードを破棄
	memset(sess->response_code, 0, RESPONSE_CODE_LENGTH);

	return 1;
}

/*
 * ログイン状態の確認
 * 引数:
 * 　sess: ログインセッション
 * 返り値:
 * 　0の時ログイン失敗あるいはログイン処理がまだ実行されていない
 * 　1の時ログイン成功
 */
int server_session_login_status(struct LoginSession* sess)
{
	return sess->status;
}

