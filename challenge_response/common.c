// サーバとクライアント両方で行う処理

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/sha.h>

#include "common.h"

/*
 * レスポンスコードの生成
 * 引数:
 * 　callenge_code: チャレンジコード
 * 　pass: パスワード
 * 　[out] response_code: 生成されたレスポンスコード
 */
void generate_response_code(char challenge_code[CHALLENGE_CODE_LENGTH], char* pass, char response_code[RESPONSE_CODE_LENGTH])
{
	int passlen;
	char* buf;

	passlen = strlen(pass);
	buf = (char*)malloc(CHALLENGE_CODE_LENGTH + passlen);
	if (!buf)
	{
		fprintf(stderr, "メモリ確保に失敗しました。\n");
		exit(EXIT_FAILURE);
	}

	buf[0] = 0;
	strncat(buf, challenge_code, CHALLENGE_CODE_LENGTH);
	strncat(buf, pass, passlen);

	SHA1((unsigned char*)buf, strlen(buf), (unsigned char*)response_code);

	free(buf);
}

