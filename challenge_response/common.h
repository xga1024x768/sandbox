#ifndef _H_COMMON_
#define _H_COMMON_

#include "challengeresponse.h"

void generate_response_code(char[CHALLENGE_CODE_LENGTH], char*, char[RESPONSE_CODE_LENGTH]);

#endif

