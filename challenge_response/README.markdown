challengeresponse
=================================================

授業でやったのでチャレンジレスポンス認証を書いた。

## 必要なもの
* OpenSSL (SHA1のハッシュ生成に使用)

## 使い方
gccの場合は次のコマンドでコンパイルできます。他のコンパイラの場合のことはよくわかりません。

gcc -Wall -o challengeresponse -lssl challengeresponse.c common.c server.c client.c

生成されたchallengeresponseを起動するとIDとパスワードを聞かれます。デフォルトではIDにuser1、パスワードにpasswordを入力するとログイン成功します。server.cのusers配列を書き換えることでログインできるユーザを増やすことができます。

## それぞれのファイルについて
* challengeresponse.{c,h}
	* クライアント側(パスワードの入力)の処理
* common.{c,h}
	* サーバ側でもクライアントでも使用する共通の処理が書かれたファイル
* server.{c,h}
	* サーバ側の処理
	* server.cに書かれているusers配列に追記することでログインできるユーザを増やせます
* client.{c,h}
	* クライアント側(特に実際のログイン部分)の処理

