// クライアント側の処理

#include "common.h"
#include "client.h"
#include "server.h"

/*
 * ログイン
 * server_から始まる関数に直接パスワードを渡していないことに注意
 * 引数:
 * 　id: ユーザID
 * 　pass: パスワード
 * 返り値:
 * 　0以外の場合ログイン成功
 * 　0の場合何らかのエラーが発生したかログイン失敗
 */
int login(char* id, char* pass)
{
	struct LoginSession* session;
	char challenge_code[CHALLENGE_CODE_LENGTH];
	char response_code[RESPONSE_CODE_LENGTH];
	int result;

	// ログインセッションを開始
	server_start_loginsession(&session);

	// チャレンジコードをもらう
	// ユーザが存在しないなど何らかのエラーが発生した時はエラー
	if (!server_request_challenge_code(session, id, challenge_code)) return 0;

	// レスポンスコードを生成
	generate_response_code(challenge_code, pass, response_code);

	// レスポンスコードを送信
	server_send_response_code(session, response_code);

	// ログイン可否を取得
	result = server_session_login_status(session);

	// セッション終了
	server_finish_loginsession(session);

	return result;
}

