// チャレンジレスポンス認証
// gcc -Wall -o challengeresponse -lssl challengeresponse.c common.c server.c client.c

#include <stdio.h>
#include <string.h>

#include "challengeresponse.h"
#include "common.h"
#include "client.h"

int main()
{
	// 入力されたIDとパスワード
	char id[10], pass[128];

	// IDの入力
	printf("ID:");
	fgets(id, 10, stdin);

	// パスワードの入力
	printf("Password:");
	fgets(pass, 128, stdin);

	// 末尾の改行を取り除く
	strtok(id, "\n");
	strtok(pass, "\n");

	// ログイン
	if (login(id, pass))
	{
		printf("ログインが成功しました。\n");
	}
	else
	{
		printf("ログインが失敗しました。\n");
	}

	return 0;
}

