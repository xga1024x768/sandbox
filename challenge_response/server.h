#ifndef _H_SERVER_
#define _H_SERVER_

/*
 * ログインセッションの情報が入った構造体
 */
struct LoginSession
{
	// レスポンスコード
	char response_code[RESPONSE_CODE_LENGTH];
	// ログイン状態
	int  status;
};

void server_start_loginsession(struct LoginSession**);
void server_finish_loginsession(struct LoginSession*);
int server_request_challenge_code(struct LoginSession*, char*, char[CHALLENGE_CODE_LENGTH]);
int server_send_response_code(struct LoginSession*, char[RESPONSE_CODE_LENGTH]);
int server_session_login_status(struct LoginSession*);

#endif

