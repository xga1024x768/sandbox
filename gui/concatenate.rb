#!/usr/bin/ruby
# -*- coding: utf-8 -*-

require 'rubygems'
require 'gtk2'

vbox = Gtk::VBox.new

label = Gtk::Label.new("2つの文字を連結")
vbox.pack_start(label)

textbox1 = Gtk::Entry.new
vbox.pack_start(textbox1)

textbox2 = Gtk::Entry.new
vbox.pack_start(textbox2)

result_textbox = Gtk::Entry.new
result_textbox.editable = false
vbox.pack_start(result_textbox)

concat_button = Gtk::Button.new("連結")
concat_button.signal_connect("clicked") do
	result_textbox.text = textbox1.text + textbox2.text
end
vbox.pack_start(concat_button)

win = Gtk::Window.new
win.signal_connect("destroy") do
	Gtk.main_quit
end
win.add(vbox)
win.show_all

Gtk.main

