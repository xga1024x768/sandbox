#!/usr/bin/ruby
# -*- coding: utf-8 -*-

require 'digest'
require 'rubygems'
require 'sinatra'
require 'erubis'

Template = DATA.read

DIGESTS = {
	"MD5"    => Digest::MD5,
	"SHA1"   => Digest::SHA1,
	"RMD160" => Digest::RMD160,
	"SHA256" => Digest::SHA256,
	"SHA384" => Digest::SHA384,
	"SHA512" => Digest::SHA512,
	"SHA2"   => Digest::SHA2,
}

get '/' do
	@plain_text  = params[:plain_text]
	@digest_type = params[:digest_type]
	if @digest_type
		if not DIGESTS.has_key?(@digest_type)
			@error_text = "ハッシュ関数の指定が不正です"
		else
			@digest = DIGESTS[@digest_type].hexdigest(@plain_text)
		end
	end
	erb Template
end

__END__
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>digest</title>
</head>
<body>
<form action="/">
<% if @error_text %>エラー: <%= @error_text %><br><% end %>
<input type="text" name="plain_text"<% if @plain_text %> value="<%= escape_html @plain_text %>"<% end %>>
<select name="digest_type">
<% DIGESTS.to_a.each do |digest_a| type = digest_a[0] %>
<option value="<%= escape_html type %>"<% if @digest_type and @digest_type == type %> selected<% end %>><%= escape_html type %></option>
<% end %>
</select>
<input type="submit" value="→ハッシュ化→">
<input type="text"<% if @digest %> value="<%= escape_html @digest %>"<% end %> style="width: 300px;" readonly>
</form>
</body>
</html>

