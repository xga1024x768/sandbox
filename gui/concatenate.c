// gcc -mwindows concatenate.c

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#define CONCAT_BUTTON 0
#define WINDOWCLASS "ConcatenateString"
#define GetInstanceHandle (HINSTANCE)GetModuleHandle(NULL)

void ChangeFont(HWND hWnd)
{
	HFONT hFont;
	hFont = GetStockObject(DEFAULT_GUI_FONT);
	SendMessage(hWnd, WM_SETFONT, (WPARAM)hFont, MAKELPARAM(FALSE, 0));
}

void AdjustHeight(HWND hWnd)
{
	HDC hdc;
	TEXTMETRIC tm;
	RECT rect;
	hdc = GetDC(hWnd);
	GetClientRect(hWnd, &rect);
	GetTextMetrics(hdc, &tm);
	SetWindowPos(hWnd, 0, 0, 0,
			rect.right-rect.left,
			tm.tmHeight,
			SWP_NOZORDER | SWP_NOMOVE
	);
	ReleaseDC(hWnd, hdc);
}

HWND CreateLabel(HWND hWnd, char* text, int x, int y, int width)
{
	HWND hLabel;
	hLabel = CreateWindowEx(
			0,
			"STATIC",
			text,
			WS_CHILD | WS_VISIBLE,
			x, y, width, 0,
			hWnd, NULL, GetInstanceHandle, NULL
	);
	ChangeFont(hLabel);
	AdjustHeight(hLabel);
	return hLabel;
}

HWND CreateTextBox(HWND hWnd, int x, int y, int width)
{
	HWND hTextBox;
	hTextBox = CreateWindowEx(
			0,
			"EDIT",
			"",
			WS_CHILD | WS_VISIBLE | WS_BORDER,
			x, y, width, 0,
			hWnd, NULL, GetInstanceHandle, NULL
	);
	ChangeFont(hTextBox);
	AdjustHeight(hTextBox);
	return hTextBox;
}

HWND CreateButton(HWND hWnd, char* text, int id, int x, int y, int width)
{
	HWND hButton;
	hButton = CreateWindowEx(
			0,
			"BUTTON",
			text,
			WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
			x, y, width, 10,
			hWnd, (HMENU)CONCAT_BUTTON, GetInstanceHandle, NULL
	);
	ChangeFont(hButton);
	AdjustHeight(hButton);
	return hButton;
}

void ConcatButtonPressed(HWND hWnd, HWND hTextBox1, HWND hTextBox2, HWND hResultTextBox)
{
	int tb1len, tb2len;
	char *tb1text, *tb2text;
	char* buf;

	tb1len = GetWindowTextLength(hTextBox1);
	tb2len = GetWindowTextLength(hTextBox2);

	tb1text = (char*)malloc(tb1len+1);
	GetWindowText(hTextBox1, tb1text, tb1len+1);
	tb2text = (char*)malloc(tb2len+1);
	GetWindowText(hTextBox2, tb2text, tb2len+1);

	buf = (char*)malloc(tb1len+tb2len+1);
	strcpy(buf, tb1text);
	strcat(buf, tb2text);

	SetWindowText(hResultTextBox, buf);

	free(tb1text);
	free(tb2text);
	free(buf);
}

LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp)
{
	static HWND hLabel;
	static HWND hTextBox1;
	static HWND hTextBox2;
	static HWND hResultTextBox;
	static HWND hConcatButton;

	switch(msg)
	{
		case WM_CREATE:
			hLabel = CreateLabel(hWnd, "2つの文字を連結", 10, 10, 100);
			hTextBox1 = CreateTextBox(hWnd, 10, 30, 80);
			hTextBox2 = CreateTextBox(hWnd, 10, 60, 80);
			hResultTextBox = CreateTextBox(hWnd, 10, 90, 160);
			hConcatButton = CreateButton(hWnd, "連結", CONCAT_BUTTON, 10, 120, 40);
			break;
		case WM_COMMAND:
			if (HIWORD(wp) == BN_CLICKED)
			{
				switch (LOWORD(wp))
				{
					case CONCAT_BUTTON:
						ConcatButtonPressed(hWnd, hTextBox1, hTextBox2, hResultTextBox);
						break;
				}
			}
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
	}
	return DefWindowProc(hWnd, msg, wp, lp);
}

ATOM RegisterWindowClass(char* className)
{
	WNDCLASSEX wc;

	wc.cbSize = sizeof(wc);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = GetInstanceHandle;
	wc.hIcon = NULL;
	wc.hIconSm = NULL;
	wc.hCursor = NULL;
	wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = className;

	return RegisterClassEx(&wc);
}

HWND CreateMyWindow(char* className, char* windowName)
{
	HWND hWnd;

	hWnd = CreateWindowEx(
			0,
			className,
			windowName,
			WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT, CW_USEDEFAULT,
			CW_USEDEFAULT, CW_USEDEFAULT,
			NULL, NULL, GetInstanceHandle, NULL
	);

	return hWnd;
}

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
	HWND hWnd;
	BOOL ret;
	MSG msg;

	if (!RegisterWindowClass(WINDOWCLASS))
	{
		fprintf(stderr, "error: RegisterWindowClass\n");
		return 0;
	}

	if (!(hWnd = CreateMyWindow(WINDOWCLASS, "test")))
	{
		fprintf(stderr, "error: CreateMyWindow\n");
		UnregisterClass(WINDOWCLASS, GetInstanceHandle);
		return 0;
	}
	SetWindowPos(hWnd, 0, 0, 0, 200, 200, SWP_NOZORDER | SWP_NOMOVE);
	ShowWindow(hWnd, nCmdShow);

	while (ret = GetMessage(&msg, NULL, 0, 0))
	{
		if (ret != -1)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	UnregisterClass(WINDOWCLASS, GetInstanceHandle);
	return 0;
}

